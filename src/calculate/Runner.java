package calculate;

import java.util.Scanner;

public class Runner {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter the first number: ");
		int first = keyboard.nextInt();
		System.out.print("Enter the second number: ");
		int second = keyboard.nextInt();
		System.out.println("Choose an operatrion");
		System.out.println("1. Add");
		System.out.println("2. Substract");
		System.out.println("3. Divide");
		System.out.println("4. Multiply");
		System.out.println("5. Power");
		System.out.print("Your choice: ");
		int operation =  keyboard.nextInt();
		if(operation == 1){
			System.out.println(first + " + "+ second + " = " + (first+second));
		}
		if(operation == 2){
			System.out.println(first + " - "+ second + " = " + (first-second));
		}
		if(operation == 3){
			//System.out.println(first + " / "+ second + " = " + (first/second));
			System.out.println(first + " / "+ second + " = " + ((double)first/second));
		}
		if(operation == 4){
			System.out.println(first + " * "+ second + " = " + (first*second));
		}
		if(operation == 5){
			System.out.println(first + " ^ "+ second + " = " + (Math.pow(first,second)));
		}
	}
}
