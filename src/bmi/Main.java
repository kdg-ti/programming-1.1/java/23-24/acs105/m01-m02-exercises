package bmi;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		System.out.println("Dear patient, this programma will calculate your BMI.");
		System.out.print("Please enter your weight in kilograms: ");
		Scanner keyboard = new Scanner(System.in);
		int weight = keyboard.nextInt();
		System.out.print("Please enter your length in meters: ");
		double length = keyboard.nextDouble();
		double bmi = weight / (length * length);
		System.out.println("Your BMI is: " + bmi);
//		if (bmi < 18) {
//			System.out.println("You are underweight.");
//		}
//		if (bmi >= 18 && bmi<25) {
//			System.out.println("You are healthy weight.");
//		}
//		if (bmi >= 25 && bmi<30) {
//			System.out.println("You are overweight.");
//		}
//		if (bmi >= 30) {
//			System.out.println("You are obese.");
//		}

		if (bmi < 18) {
			System.out.println("You are underweight.");
		} else if (bmi < 25) {
			System.out.println("You are healthy weight.");
		} else if (bmi < 30) {
			System.out.println("You are overweight.");
		} else {
			System.out.println("You are obese. Do something");
		}
	}
}