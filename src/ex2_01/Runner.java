package ex2_01;

import java.util.Scanner;

public class Runner {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		boolean a = true ;
		char b = 'A';
		byte c = 0;
		short d = 10000;
		int e = 505;
		long f= 54;
		float g = 1.2f;
		double h = 22.22;
		final double PI= 3.14;
		// you cannot reassing a final variable
		//PI = 3.145697;


		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);
		System.out.println(g);
		System.out.println(f);
		System.out.println(h);
		System.out.println(PI);
		char unicode = '\uf531';
		System.out.println(unicode);

	}
}
