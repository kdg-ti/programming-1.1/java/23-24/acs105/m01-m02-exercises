package predict_operators;

public class Runner {
	public static void main(String[] args) {
		int a = 10;
		int b = 3;
		a+=b;
		System.out.println(a + " " + b);

		b+=a;
		System.out.println(a + " " + b);

		b++;
		System.out.println(a + " " + b);

		++a;
		System.out.println(a + " " + b);

		System.out.println(a++);
		System.out.println(++b);
		System.out.println(++b + a++);


	}
}
