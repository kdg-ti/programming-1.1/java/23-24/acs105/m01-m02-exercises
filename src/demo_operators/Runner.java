package demo_operators;

public class Runner {
	public static void main(String[] args) {
		int i = 5;
		int j = 3;
		System.out.println("Modulo operator shows the remainder: " + i % j);
		j++;
		System.out.println("j++: " + j);

		// success for j++
		// failure for ++j (because we increment before comparing)
		if (i > j++) {
			System.out.println("success, j = " + j);
		} else {
			System.out.println("failure, j = " + j);
		}
		int k = -j;
		System.out.println(k);
		if (k == -j) {
			System.out.println("equal");
		}
		if (k != j) {
			System.out.println("not equal");
		}

		if (!(k == j)) {
			System.out.println("not equal");
		}

		i = 10;
		System.out.println("i = " + i + ", j= " + j);
		int tester;
		if (i > j) tester = i;
		else tester = j;
		System.out.println("test assignment: " + tester);

		tester = (i < j) ? 45 : 36;
		System.out.println("test assignment: " + tester);


	}
}
